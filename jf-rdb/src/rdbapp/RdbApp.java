package rdbapp;

import java.awt.Menu;
import java.time.Duration;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Scanner;

import rdbapp.model.Account;
import rdbapp.model.Client;
import rdbapp.services.AccountService;
import rdbapp.services.AccountServiceImpl;
import rdbapp.services.ClientService;
import rdbapp.services.ClientServiceImpl;
import rdbapp.ui.Menus;
import rdbapp.ui.MenusImpl;
import rdbapp.services.BankService;
import rdbapp.services.BankServiceImpl;

public class RdbApp {
	
	public static Long activeClientId;
	
	public static void main(String[] args) {

		
		BankService init = new BankServiceImpl();

		init.main();
	
//		while (true) {
//			System.out.println("teste");
//			
//			try {
//				System.out.print("Inside try: ");
//				Scanner sc = new Scanner(System.in);
//				int input = sc.nextInt();
//				break;
//				
//			} catch (Exception e) {
//				System.out.println("Inside catch with continue");
//				continue;
//			}
//			//System.out.println("after try");
//		}
//		
//		System.out.println("after while");
		
	}

	// account testing

//	public static void main(String[] args) {
//		ClientService clientservice = new ClientServiceImpl();
//		
//		Client newClient = new Client();
//		newClient.setName("Jo�o Fidalgo");
//		newClient.setBirthdate(LocalDate.of(1010, 7, 25));
//		newClient.setNif(2162130);
//		
//		Client saveClient = clientservice.save(newClient);
//		
//		
//		AccountService accountservice = new AccountServiceImpl();
//
//		Account newAccount = new Account();
//		
//		newAccount.setId(saveClient.getId());
//		newAccount.setMainOwner(newClient);
//		newAccount.setBalance(50D);
//	
//		Account saveAccount = accountservice.add(newAccount);
//		
//		Account newAccount1 = new Account();
//		newAccount1.setBalance(60D);
//		saveAccount = accountservice.add(newAccount1);
//		
//		System.out.println(Arrays.asList(accountservice.getAll()));
//		
//	}

	// client testing

//	public static void main(String[] args) {
//
//		ClientService clientservice = new ClientServiceImpl();
//		
//		Client saveClient = null;
//		
//		Client newClient = new Client();
//		newClient.setName("Jo�o Fidalgo");
//		newClient.setBirthdate(LocalDate.of(1010, 7, 25));
//		newClient.setNif(2162130);
//		
//		Client newClient1 = new Client();
//		newClient1.setName("Jo�o");
//		newClient1.setBirthdate(LocalDate.of(2001, 7, 26));
//		newClient1.setNif(2162131);
//
//		Client newClient2 = new Client();
//		newClient2.setName("Jo�o Duarte");
//		newClient2.setBirthdate(LocalDate.of(2001, 7, 27));
//		newClient2.setNif(21621301);
//
//		
//		saveClient = clientservice.save(newClient);
//		
//		System.out.println(saveClient);
//
//		saveClient = clientservice.save(newClient1);
//		
//		System.out.println(saveClient);
////		
//		saveClient = clientservice.save(newClient2);
//		
//		System.out.println(saveClient);
//		
//			//System.out.println(Arrays.asList(clientservice.getAll()));
//			//System.out.println(clientservice.getAll().size());
//
//			
//		// the method to update database must be done with a temp client object.
//		// probably the update method must have a current user and a new data to update object.
//		// that way when you update the birthdate, you are not writting direct to object reference, but the
//		// update method will do that work.
//		
//		Client tempClient = new Client();
//		
//		tempClient.setId(newClient1.getId());
//		tempClient.setName("Jo�o Duarte Fidalgo");
//		tempClient.setBirthdate(LocalDate.of(2010, 7, 25));
//
//		//Client updateClient = ; // this is not checking underage. why?!?!
//		
//		System.out.println("Update " + clientservice.update(newClient1, tempClient));
//		System.out.println();
//		System.out.println(Arrays.asList(clientservice.getAll()));
////		System.out.println(clientservice.getAll().size());
//
//		
////		System.out.println(clientservice.delete(newClient1.getId()));
//
//		
//		
//	}

}
