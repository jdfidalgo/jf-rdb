package rdbapp.database;

import java.util.List;

import rdbapp.model.Account;
import rdbapp.model.Client;

public interface AccountDatabase {
	
	// there will be no delete account. That will require the user to be deleted.
	
	public Account add(Account newAccount);
	
	public Account addSecondary(Account currentAccount, Long clientId);
	
	public Boolean delete(String accountNumber);
	
	public Account update(Account currentAccount);
	
	public Account find(String accountNumber);
	
	public List<Account> find(Long clientId);
	
	public List<Account> findSecondaries(Long clientId);
	
	public Double increaseBalance(Account currentAccount, Double depositCash);
	
	public Double decreaseBalance(Account currentAccount, Double widrawCash);
	
	public Double getBalance(Account currentAccount);
	
	public List<Account> getAll();
	
	//public List<Long> getAllSecondariesById(Account currentAccount);
	
}
