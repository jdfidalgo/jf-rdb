package rdbapp.database;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import rdbapp.model.Account;

public class AccountDatabaseImpl implements AccountDatabase {
	
	private List<Account> accounts = new ArrayList<Account>(); // MY DATABASE - this will be used throughout all classes;
	// account number is unique
	
	@Override
	public Account add(Account newAccount) {
		
		String accountNumber = accountNumberGenerator(); // account number will be a random generated number. It is not in the clients control
		
		if (accounts.size() != 0) { // Accounts exist. Must check if generated number is unique
			while (true) {
				if (isAccountNumberUnique(accountNumber)) {
					break;
				} else {
					accountNumber = accountNumberGenerator();
				}
			}
		}
		
		newAccount.setAccountNumber(accountNumber); // this is forced by the database. Client cannot set this
		accounts.add(newAccount);
		
		return newAccount;
	}
	
	public Account addSecondary(Account currentAccount, Long clientId) {
		// Checks for empty database. Might not be necessary, but check is still done
		if (accounts.size() == 0) { return null; }
		
		List<Long> clientsId = new ArrayList<Long>(currentAccount.getSecondaryOwners());
		
		//clientsId = currentAccount.getSecondaryOwners();
		
		clientsId.add(clientId);
		currentAccount.setSecondaryOwners(clientsId);
		
		return currentAccount;
		
	}

	@Override
	public Boolean delete(String accountNumber) {
		
		// Checks for empty database. Might not be necessary, but check is still done
		if (accounts.size() == 0) { return null; }
		
		for (int i = 0 ; i < accounts.size(); i++) {
			if (accounts.get(i).getAccountNumber().equals(accountNumber)) {
				accounts.remove(i);
				return true;
			}
		}
		return false; // no account found for provided account number
	}
	
	// This method will be used for mainly adding and removing secondary users
	@Override
	public Account update(Account currentAccount) {
		
		String accountToUpdate = currentAccount.getAccountNumber();
		
		for (int i = 0 ; i < accounts.size(); i++) {
			if (accounts.get(i).getAccountNumber().equals(accountToUpdate)) {
				accounts.set(i, currentAccount);
				return currentAccount;
			}
		}
		return null; // no account was found
	}
	
	@Override
	public Account find(String accountNumber) {
		
		if (accounts.size() != 0) {
			for (int i = 0 ; i < accounts.size(); i++) {
				if (accounts.get(i).getAccountNumber().equals(accountNumber)) {
					return accounts.get(i);
				}
			}
		}
		return null; // no account exists or no account found.
	}
	
	@Override
	public List<Account> find(Long clientId) {
		List myList = new ArrayList();
		
		if (accounts.size() != 0) {
			for (int i = 0 ; i < accounts.size(); i++) {
				if (accounts.get(i).getId() == clientId) {
					myList.add(accounts.get(i));
				}
			}
		}
		return myList;
	}
	
	public List<Account> findSecondaries(Long clientId) {
		List<Account> myList = new ArrayList<Account>();
		
		if (accounts.size() != 0) {
			try {
				for (int i = 0 ; i < accounts.size(); i++) {
					for (Long secondaryIds : accounts.get(i).getSecondaryOwners()) {
					//for (int j = 0 ; j < accounts.get(i).getSecondaryOwners().size(); j++) {
						if (secondaryIds == clientId) {
							myList.add(accounts.get(i));
						}
					}
	
				}
			} catch (NullPointerException e) {
				return null; // no secondaries exist
			}
		}
		return myList;
	}

	@Override
	public Double increaseBalance(Account currentAccount, Double depositCash) {
		
		if (accounts.size() != 0) {
			
			Account myAccount = find(currentAccount.getAccountNumber());
			
			if (myAccount == null) { return null; } // no account found
			else {
				Double currentBalance = myAccount.getBalance();
				myAccount.setBalance(currentBalance + depositCash); 
				return currentBalance + depositCash;
			}
		}
		return null; // no accounts exist
	}

	@Override
	public Double decreaseBalance(Account currentAccount, Double widrawCash) { // must check if negative balance is allowed!!!
		
		if (accounts.size() != 0) {
			
			Account myAccount = find(currentAccount.getAccountNumber());
			
			if (myAccount == null) { return null; } // no account found
			else {
				Double currentBalance = myAccount.getBalance();
				myAccount.setBalance(currentBalance - widrawCash); 
				return currentBalance - widrawCash;
			}
		}
		return null; // no accounts exist
	}
	
	@Override
	public Double getBalance(Account currentAccount) {
		if (accounts.size() != 0) {
			
			Account myAccount = find(currentAccount.getAccountNumber());
			
			if (myAccount == null) { return null; } // no account found
			else { return myAccount.getBalance(); }
		}
		return null; // no accounts exist
	}
	
	
	public List<Account> getAll() {
		return accounts;
	}
	
//	public List<Long> getAllSecondariesById(Account currentAccount) {
//		List<Long> secondaries = new ArrayList<Long>(currentAccount.getSecondaryOwners());
//		return secondaries;
//	}
	
	
	private String accountNumberGenerator() {
	
		SecureRandom random = new SecureRandom();
		String myAccountNumber = "";
		Integer accountNumberSize = 15; // with trailers: "-" @ positions 4 and 11 (random selected)
		
		for (int i = 0; i <= accountNumberSize; i++) {
			
			if (i == 3 || i == 10) { myAccountNumber += "-"; continue;}
			
			myAccountNumber += String.valueOf(random.nextInt(9));
		}
		return myAccountNumber;
	}
	
	private Boolean isAccountNumberUnique(String checkAccountNumber) {
		// there is always a chance that the random generates the same number.
		
		//if (accounts.size() == 0) { return null; } // no accounts exist
		
		// Call method only when there are accounts
		for (int i = 0; i < accounts.size(); i++) {
			if (accounts.get(i).getAccountNumber().equals(checkAccountNumber)) { return false; }
		}
		return true;
	}
	

}
