package rdbapp.database;

import java.util.List;

import rdbapp.model.Client;

public interface ClientDatabase {
	
	public Client save(Client newClient);
	
	public Client update(Client currentClient);
	
	public Client find(Long clientId);
	
	public List<Client> getAll();
	
	public Boolean delete(Long clientId); // when possible, methods will return true or false to state if operation was successful
	
}
