package rdbapp.database;

import java.time.Duration;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import rdbapp.model.Client;

public class ClientDatabaseImpl implements ClientDatabase {

	private List<Client> clients = new ArrayList<Client>(); // MY DATABASE - this will be used throughout all classes;
	private Long id = 0L; // this id will be used throughout all classes
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public Client save(Client newClient) {
		
		newClient.setId(getId());
		clients.add(newClient);

		setId(getId() + 1);
		return newClient;
	}

	@Override
	public Client update(Client currentClient) {
		
		for (int i = 0; i < clients.size(); i++) {
			
			Long existingClientId = clients.get(i).getId();
			Long currentClientId = currentClient.getId();

			if (currentClientId.equals(existingClientId)) {
				clients.set(i, currentClient);
				return currentClient;
			}
		}

		return null;
	}

	@Override
	public Client find(Long clientId) {
		for (int i = 0; i < clients.size(); i++) {
			if (clients.get(i).getId() == clientId) {
				return clients.get(i);
			}
		}
		return null;
	}
	
	@Override
	public List<Client> getAll() {
		return clients;
	}

	@Override
	public Boolean delete(Long clientId) {
		// Check must be done since we can delete someone even with the database empty;
		// - to be reviewed
		if (clients.size() == 0) {
			return null;
		} // database is empty - method will return null

		for (int i = 0; i < clients.size(); i++) {
			if (clients.get(i).getId() == clientId) {
				clients.remove(i);
				// id--;  // this isnt done in the industry standards
				return true;
			}
		}
		return false;
	}
}
