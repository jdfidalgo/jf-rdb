package rdbapp.database;

import java.util.List;

import rdbapp.model.CreditCard;
import rdbapp.model.DebitCard;

public interface CreditCardDatabase {
	
	// Extension just serves to get setPinNumber.
	// In a next version maybe there is no need to extend debit card to credit
	
	public Double increasePlafond(String creditCardNumber, Double depositPlafound);
	
	public Double decreasePlafond(String creditCardNumber, Double withdrawPlafound);
	
	public CreditCard add(CreditCard newCreditCard);
	
	public Boolean remove(String creditCardNumber);
	
	public CreditCard find(Long clientId);
	
	public List<CreditCard> getAll(Long clientId);
	
	// get or set plafound is already from the object model
	// get or set number of withdraws is from the object model
}
