package rdbapp.database;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

import rdbapp.model.CreditCard;
import rdbapp.model.DebitCard;

public class CreditCardDatabaseImpl implements CreditCardDatabase {
	
	private List<CreditCard> creditCards = new ArrayList<CreditCard>(); // MY DATABASE - this will be used throughout all classes;
	//private Long id = 0L; // this id will be used throughout all classes
	
	@Override
	public CreditCard add(CreditCard newCreditCard) {
		
		String creditCardNumber = cardNumberGenerator("D");
		
		if (creditCards.size() != 0) { // Credit cards exist. Must check if generated number is unique
			while (true) {
				if (isCardNumberUnique(creditCardNumber)) {
					break;
				} else {
					creditCardNumber = cardNumberGenerator("D");
				}
			}
		}
		
		newCreditCard.setCardNumber(creditCardNumber);;// this is forced by the database. Client cannot set this
		creditCards.add(newCreditCard);
		
		return newCreditCard;
	}

	@Override
	public Boolean remove(String creditCardNumber) {
		// Checks for empty database. Might not be necessary, but check is still done
		if (creditCards.size() == 0) { return null; } // nothing to remove
		
		for (int i = 0 ; i < creditCards.size(); i++) {
			if (creditCards.get(i).getCardNumber().equals(creditCardNumber)) {
				creditCards.remove(i);
				return true;
			}
		}
		return false; // no account found for provided account number
	}
	
	@Override
	public CreditCard find(Long clientId) {
		
		if (creditCards.size() == 0) { return null; } // there are no debit cards
		
		for (CreditCard creditCard : this.creditCards) {
			if (creditCard.getId() == clientId) {
				return creditCard;
			}
		}
		return null; // nothing found
	}

	@Override
	public List<CreditCard> getAll(Long clientId) {
		
		if (creditCards.size() == 0) { return null; } // there are no debit cards
		
		List creditCardList = new ArrayList();
		
		for (CreditCard creditCard : this.creditCards) {
			if (creditCard.getId() == clientId) {
				creditCardList.add(creditCard);
			}
		}
		return creditCardList; // if no cards found, then will return a null list
	}
	

	
	@Override
	public Double increasePlafond(String creditCardNumber, Double depositPlafound) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Double decreasePlafond(String creditCardNumber, Double withdrawPlafound) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public Boolean setPinNumber(CreditCard creditCard, String pinNumber) {
		// should be a valid credit card (object)
		try {
			creditCard.setPin(pinNumber);
		} catch (Exception e) {
			return false;
		}
		return true;	
	}
	
	private Boolean isCardNumberUnique(String checkCardNumber) {

		for (int i = 0; i < creditCards.size(); i++) {
			if (creditCards.get(i).getCardNumber().equals(checkCardNumber)) { return false; }
		}
		return true;
	}

	private String cardNumberGenerator(String cardTypeFirstLetter) {
		
		// Method signature must be "D" or "C"
		if (cardTypeFirstLetter != "D" || cardTypeFirstLetter != "C") { return null; } // invalid signature input
		
		SecureRandom random = new SecureRandom();
		String myCardNumber = cardTypeFirstLetter;
		Integer cardNumberSize = 10;
		
		for (int i = 0; i <= cardNumberSize; i++) {
			myCardNumber += String.valueOf(random.nextInt(9));
		}
		return myCardNumber;
	}
}
