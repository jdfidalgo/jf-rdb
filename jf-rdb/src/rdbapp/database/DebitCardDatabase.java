package rdbapp.database;

import java.util.List;

import rdbapp.model.Account;
import rdbapp.model.Client;
import rdbapp.model.CreditCard;
import rdbapp.model.DebitCard;

public interface DebitCardDatabase {
	
	public DebitCard add(DebitCard newDebitCard);
	
	public Boolean remove(String debitCardNumber);
	
	public Boolean setPinNumber(DebitCard debitCard, String pinNumber);
	
	public DebitCard find(Long clientId);
	
	public List<DebitCard> getAll(Long clientId);
	

//	public Long id;
//	public String cardNumber;
//	public Client owner;
//	public Account accountNumber;
//	public String pin;
//	public Double dailyBalance;
	
}
