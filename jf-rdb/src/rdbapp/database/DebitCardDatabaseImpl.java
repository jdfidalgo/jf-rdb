package rdbapp.database;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

import rdbapp.model.Client;
import rdbapp.model.DebitCard;

public class DebitCardDatabaseImpl implements DebitCardDatabase {

	private List<DebitCard> debitCards = new ArrayList<DebitCard>(); // MY DATABASE - this will be used throughout all classes;
	//private Long id = 0L; // this id will be used throughout all classes
	
	@Override
	public DebitCard add(DebitCard newDebitCard) {
		
		String debitCardNumber = cardNumberGenerator("D");
		
		if (debitCards.size() != 0) { // Debit cards exist. Must check if generated number is unique
			while (true) {
				if (isCardNumberUnique(debitCardNumber)) {
					break;
				} else {
					debitCardNumber = cardNumberGenerator("D");
				}
			}
		}
		
		newDebitCard.setCardNumber(debitCardNumber);// this is forced by the database. Client cannot set this
		debitCards.add(newDebitCard);
		
		return newDebitCard;
	}

	@Override
	public Boolean remove(String debitCardNumber) {
		// Checks for empty database. Might not be necessary, but check is still done
		if (debitCards.size() == 0) { return null; } // nothing to remove
		
		for (int i = 0 ; i < debitCards.size(); i++) {
			if (debitCards.get(i).getCardNumber().equals(debitCardNumber)) {
				debitCards.remove(i);
				return true;
			}
		}
		return false; // no account found for provided account number
	}

	
	@Override
	public DebitCard find(Long clientId) {
		
		if (debitCards.size() == 0) { return null; } // there are no debit cards
		
		for (DebitCard debitCard : this.debitCards) {
			if (debitCard.getId() == clientId) {
				return debitCard;
			}
		}
		return null; // nothing found
	}

	@Override
	public List<DebitCard> getAll(Long clientId) {
		
		if (debitCards.size() == 0) { return null; } // there are no debit cards
		List debitCardList = new ArrayList();
		
		for (DebitCard debitCard : this.debitCards) {
			if (debitCard.getId() == clientId) {
				debitCardList.add(debitCard);
			}
		}
		return debitCardList; // if no cards found, then will return a null list
	}
	
	@Override
	public Boolean setPinNumber(DebitCard debitCard, String pinNumber) {
		// should be a valid debit card (object)
		try {
			debitCard.setPin(pinNumber);
		} catch (Exception e) {
			return false;
		}
		return true;	
	}
	
	private String cardNumberGenerator(String cardTypeFirstLetter) {
		
		// Method signature must be "D" or "C"
		if (cardTypeFirstLetter != "D" || cardTypeFirstLetter != "C") { return null; } // invalid signature input
		
		SecureRandom random = new SecureRandom();
		String myCardNumber = cardTypeFirstLetter;
		Integer cardNumberSize = 10;
		
		for (int i = 0; i <= cardNumberSize; i++) {
			myCardNumber += String.valueOf(random.nextInt(9));
		}
		return myCardNumber;
	}

	private Boolean isCardNumberUnique(String checkCardNumber) {

		for (int i = 0; i < debitCards.size(); i++) {
			if (debitCards.get(i).getCardNumber().equals(checkCardNumber)) { return false; }
		}
		return true;
	}


}
