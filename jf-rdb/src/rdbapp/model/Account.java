package rdbapp.model;

import java.util.List;

public class Account {
	
	private Long id;
	private String accountNumber;
	private Client mainOwner;
	//private List<Client> secondaryOwners;
	private List<Long> secondaryOwners;
	private Double balance;
	
	public Long getId() { return id; }
	public void setId(Long id) { this.id = id; }
	
	public String getAccountNumber() { return accountNumber; }
	public void setAccountNumber(String accountNumber) { this.accountNumber = accountNumber; }
	
	public Client getMainOwner() { return mainOwner; }
	public void setMainOwner(Client mainOwner) { this.mainOwner = mainOwner; }
	
	//public List<Client> getSecondaryOwners() { return secondaryOwners; }
	//public void setSecondaryOwners(List<Client> secondaryOwners) {	this.secondaryOwners = secondaryOwners;	}
	
	public List<Long> getSecondaryOwners() { return secondaryOwners; }
	public void setSecondaryOwners(List<Long> secondaryOwners) {	this.secondaryOwners = secondaryOwners;	}
	
	public Double getBalance() { return balance; }
	public void setBalance(Double balance) { this.balance = balance; }
	
	@Override
	public String toString() {
		return "Account [id=" + id + ", accountNumber=" + accountNumber + ", mainOwner=" + mainOwner
				+ ", secondaryOwners=" + secondaryOwners + ", balance=" + balance + "]";
	}
	
	


}
