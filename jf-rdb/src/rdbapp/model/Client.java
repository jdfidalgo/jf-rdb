package rdbapp.model;

import java.time.LocalDate;

public class Client {

	// general purpose for DB "table index"
	private Long id;

	// will be used for login
	private Integer nif; // cannot start with 0
	private String password;

	// the rest of client "properties"
	private String name;
	private LocalDate birthdate; // format is YYYY-MM-DD
	private Integer phonenumber; // 9 digits - cannot start with 0
	private Integer cellnumber; // 9 digits - cannot start with 0
	private String email;
	private String job;

	public Long getId() { return id; }
	public void setId(Long id) { this.id = id; }

	public Integer getNif() { return nif; }
	public void setNif(Integer nif) { this.nif = nif; }

	public String getPassword() { return password; }
	public void setPassword(String password) { this.password = password; }

	public String getName() { return name; }
	public void setName(String name) { this.name = name; }

	public LocalDate getBirthdate() { return birthdate; }
	public void setBirthdate(LocalDate birthdate) { this.birthdate = birthdate; }

	public Integer getPhoneNumber() { return phonenumber; }
	public void setPhoneNumber(Integer phonenumber) { this.phonenumber = phonenumber; }

	public Integer getCellNumber() { return cellnumber; }
	public void setCellNumber(Integer cellnumber) { this.cellnumber = cellnumber; }

	public String getEmail() { return email; }
	public void setEmail(String email) { this.email = email; }

	public String getJob() { return job; }
	public void setJob(String job) { this.job = job; }
	
	@Override
	public String toString() {
		return "Clients [id=" + id + ", nif=" + nif + ", password=" + password + ", name=" + name + ", birthdate="
				+ birthdate + ", phonenumber=" + phonenumber + ", cellnumber=" + cellnumber + ", email=" + email
				+ ", job=" + job + "]";
	}

}
