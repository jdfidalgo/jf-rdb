package rdbapp.model;

public class CreditCard extends DebitCard {

	// Credit: cliente, conta, n�mero �nico, PIN, n�mero de levantamento di�rios,
	
	private Double plafond;
	private Integer numOfWithdrawals;
	
	public Double getPlafond() { return plafond; }
	public void setPlafond(Double plafond) { this.plafond = plafond;}
	
	public Integer getNumOfWithdraws() { return numOfWithdrawals;}
	public void setNumOfWithdraws(Integer numOfWithdrawals) { this.numOfWithdrawals = numOfWithdrawals;}
	
	@Override
	public String toString() {
		return "CreditCard [id=" + id + ", cardNumber=" + cardNumber + ", owner=" + owner + ", accountNumber=" + account + ", pin=" + pin + "]"	+ "[plafond=" + plafond + ", numOfWithdraws=" + numOfWithdrawals + "]";
	}
	
}
