package rdbapp.model;

public class DebitCard {
	
	// Debit: cliente, conta, n�mero �nico, PIN, valor j� levantado hoje (dia)
	// Credit: cliente, conta, n�mero �nico, PIN, n�mero de levantamento di�rios,
	
	public Long id;
	public String cardNumber; // to make it easier, card number can have a D(ebit) or a C(redit) at the beginning
	public Client owner;
	public Account account;
	public String pin;
	
	//public Double dailyBalance; // will be the drawn money;

//	public Double getDailyBalance() { return dailyBalance; }
//	public void setDailyBalance(Double dailyBalance) { this.dailyBalance = dailyBalance; }
	
//	@Override
//	public String toString() {
//		return "DebitCard [id=" + id + ", cardNumber=" + cardNumber + ", owner=" + owner + ", accountNumber="
//				+ accountNumber + ", pin=" + pin + ", dailyBalance=" + dailyBalance + "]";
//	}
	
	public Long getId() { return id; }
	public void setId(Long id) { this.id = id; }
	
	public String getCardNumber() { return cardNumber; }
	public void setCardNumber(String cardNumber) { this.cardNumber = cardNumber; }
	
	public Client getOwner() { return owner; }
	public void setOwner(Client owner) { this.owner = owner; }
	
	public Account getAccountNumber() { return account; }
	public void setAccountNumber(Account account) { this.account = account; }
	
	public String getPin() { return pin; }
	public void setPin(String pin) { this.pin = pin; }
	
	@Override
	public String toString() {
		return "DebitCard [id=" + id + ", cardNumber=" + cardNumber + ", owner=" + owner + ", accountNumber="
				+ account + ", pin=" + pin + "]";
	}
	
	
	
}
