package rdbapp.services;

import java.util.List;

import rdbapp.model.Account;

public interface AccountService {
	
	// AccountService will take in order that a client cannot have zero accounts.
	
	public Account add(Account newAccount);
	
	public Account find(String accountNumber);
	
	public List<Account> find(Long clientId);
	
	public Boolean delete(String accountNumber);
	
	public Account update(Account currentAccount);
	
	public Double increaseBalance(Account currentAccount, Double depositCash);
	
	public Double decreaseBalance(Account currentAccount, Double widrawCash);
	
	public Double getBalance(Account currentAccount);
	
	public List<Account> getAll();
	
	public List<Account> findSecondaries(Long clientId);
	
}
