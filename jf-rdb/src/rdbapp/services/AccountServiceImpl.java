package rdbapp.services;

import java.util.List;

import rdbapp.database.AccountDatabase;
import rdbapp.database.AccountDatabaseImpl;
import rdbapp.model.Account;

public class AccountServiceImpl implements AccountService {

	private AccountDatabase accountdatabase = new AccountDatabaseImpl();

	// Setting up an account means no cards are associated. This check is not needed here
	// When setting up an account, random number will be generated in database
	// When setting up an account, a client must be associated to it - id!!!
	// When setting up an account, only after account is setup, can secondary owners be added;
	@Override
	public Account add(Account newAccount) { 
		
		// Check account balance. If under 50�, then no account can be added / created
		if (newAccount.getBalance() < 50) { return null; }
		
		return accountdatabase.add(newAccount);
	}

	@Override
	public Account find(String accountNumber) {
		return accountdatabase.find(accountNumber);
	}
	
	@Override
	public List<Account> find(Long clientId) {
		return accountdatabase.find(clientId);
	}
	
	@Override
	public List<Account> findSecondaries(Long clientId) {
		return accountdatabase.findSecondaries(clientId);
	}

	@Override
	public Boolean delete(String accountNumber) {	
		return accountdatabase.delete(accountNumber);
	}
	
	// This method will be used mainly to update accounts as secondary users are added or removed;
	@Override
	public Account update(Account currentAccount) {
		return accountdatabase.update(currentAccount);
	}
	
	@Override
	public Double increaseBalance(Account currentAccount, Double depositCash) {
		return accountdatabase.increaseBalance(currentAccount, depositCash);
	}

	@Override
	public Double decreaseBalance(Account currentAccount, Double widrawCash) {
		return accountdatabase.decreaseBalance(currentAccount, widrawCash);
	}

	@Override
	public Double getBalance(Account currentAccount) {
		return accountdatabase.getBalance(currentAccount);
	}

	@Override
	public List<Account> getAll() {
		return accountdatabase.getAll();
	}
	


}
