package rdbapp.services;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.IllegalFormatException;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;
import java.util.UnknownFormatFlagsException;

import rdbapp.RdbApp;
import rdbapp.model.Account;
import rdbapp.model.Client;
import rdbapp.ui.Menus;
import rdbapp.ui.MenusImpl;

public class BankServiceImpl implements BankService {

	private ClientService clientservice = new ClientServiceImpl();
	private AccountService accountservice = new AccountServiceImpl();

	private Client currentClient = new Client(); // signin client
	//List<Account> currentClientAccounts = new ArrayList<Account>(); // signin client accounts
	private Long currentClientId;
	
	private ArrayList<String> clientCardNumbers = new ArrayList<String>(2); // we know that only 2 cards are allowed per client
	private Long numberOfSecondaryClients = 4L;
	
	private Menus showMenu = new MenusImpl();
	private Scanner input = new Scanner(System.in);
	
	// could be a good place to seed some client/account/card data for testing!
	
	@Override
	public void main() {
		// injects 2 clients

		Client newClient1 = new Client();
		Account newAccount1 = new Account();
		
		newClient1.setBirthdate(LocalDate.of(1980, 7, 25));
		newClient1.setCellNumber(111111111);
		newClient1.setPhoneNumber(111111111);
		newClient1.setEmail("ola@adeus.com");
		newClient1.setJob("Nothing");
		newClient1.setName("João");
		newClient1.setNif(111111111);
		newClient1.setPassword("0");
		
		Client savedClient = clientservice.save(newClient1);
		
		newAccount1.setId(savedClient.getId());
		newAccount1.setBalance(50D);
		
		Account savedAccount = accountservice.add(newAccount1);
		
		
		Client newClient2 = new Client();
		Account newAccount2 = new Account();
		
		newClient2.setBirthdate(LocalDate.of(1980, 7, 25));
		newClient2.setCellNumber(111111111);
		newClient2.setPhoneNumber(111111111);
		newClient2.setEmail("ola@adeus.com");
		newClient2.setJob("Nothing");
		newClient2.setName("Fidalgo");
		newClient2.setNif(111111112);
		newClient2.setPassword("0");
		
		savedClient = clientservice.save(newClient2);
		
		newAccount2.setId(savedClient.getId());
		newAccount2.setBalance(60D);
		
		savedAccount = accountservice.add(newAccount2);
		
		showMenu.main();

		Integer mainInput;
		
		while (true) {
			
			try {
				System.out.print(">> ");
				mainInput = input.nextInt();
				
				if (mainInput.equals("") || mainInput == null) { throw new Exception("Null input"); }
				
			} catch (InputMismatchException e) {
				System.out.println();
				System.out.println("» ERROR: incorrect input «");
				System.out.println();
				input.next(); // clear scanner so that an infinite loop doesn't occur
				continue;
			} catch (Exception e) {
				input.next(); // clear scanner so that an infinite loop doesn't occur
				continue;
			}
			
			switch (mainInput) {
			case 0:
				System.out.println();
				System.out.println("Thank you for your visit to your services!");

				System.exit(0);
			case 1:
				digitalBankMain();
				break;
			case 2:
				System.out.println(2);
				break;

			}
		}
	}
	
	@Override
	public void digitalBankMain() {
		
		showMenu.digitalBankMain();
		
		Integer digitalBankMain = null;
		Boolean menuCycle = true;
		
		while (menuCycle) {
			
			try {
				System.out.print(">> ");

				digitalBankMain = input.nextInt();

				if (digitalBankMain.equals("") || digitalBankMain == null) { throw new Exception("Null input"); }
				
			}  catch (InputMismatchException e) {
				System.out.println();
				System.out.println("» ERROR: incorrect input «");
				System.out.println();
				input.next(); // clear scanner so that an infinite loop doesn't occur
				continue;
			}
			catch (Exception e) {
				input.next(); // to avoid infinite loops
				continue;
			}

			switch (digitalBankMain) {
			case 0:
				System.out.println();
				menuCycle = false;
				break;
			case 1: // for sign in
				
				// When signing in, if success, then the method will call the front end digital bank.
				// if not, it returns the stack here with error message
				// So, the front end is never called here
				digitalBankSignIn();
				break;
			case 2: // for sign up
				
				digitalBankSignUp();
				// when sighing up, the client is logged.
				// so must go to digital bank front
				digitalBankFront();
				// front menu is shown again for better client visibility of usage
				showMenu.digitalBankMain();
				break;
			}
		}
		main();
	}
	
	@Override
	public void digitalBankSignIn() {
		
		showMenu.digitalBankSignIn();
		
		// this is to get all the clients, accounts and cards (if any)
		List<Client> allClients = new ArrayList<Client>();
		List<Account> allAccounts = new ArrayList<Account>();
		
		allClients = clientservice.getAll();
		allAccounts = accountservice.getAll();
		
		String nif;
		Integer parsedNif;
		String pass;
		
		Boolean loginSucess = false;
		final Integer attempts = 3;
		Integer currentAttempt = attempts;
		
		while (true) {
			
			input.nextLine();
			System.out.print("NIF (" + currentAttempt + ")>> ");
			nif = input.nextLine();
			
			try { // overshot. This "error" handle doesn't need to be so eloquent
				if (nif.equals("") || nif == null) { throw new Exception("Null input"); }
				
				parsedNif = Integer.parseInt(nif);
				
			} catch (NumberFormatException e) {
				System.out.println();
				System.out.println("» ERROR: incorrect input «");
				System.out.println();
				continue;
			} catch (Exception e) {
				continue; // When no user input is done, just continues in while.
			}
			
			System.out.print("PASSWORD (" + currentAttempt + ")>> ");
			pass = input.nextLine();
			
			try { // overshot. This "error" handle doesn't need to be so eloquent
				if (nif.equals("") || nif == null) { throw new Exception("Null input"); }
			} catch (Exception e) {
				continue; // When no user input is done, just continues in while.
			}

			
			// checks sign-in data

			for (Client checkClient : allClients) {
				if (parsedNif.equals(checkClient.getNif()) && pass.equals(checkClient.getPassword())) {
				
					// login successful
					currentClient = checkClient;
					currentClientId = currentClient.getId();
					loginSucess = true;
					break;
				} else { loginSucess = false; }
			}
			
			if (loginSucess == true) {
				digitalBankFront();
				break;
				
			} else { 
				--currentAttempt;
				if (currentAttempt == 0) {
					System.out.println();
					System.out.println("» ERROR: all attemps to sign-in failed «");
					System.out.println();
					// re-shows menu for better visualization
					showMenu.digitalBankMain();
					break;
				} else { System.out.println("» ERROR: incorrect sign-in data «");
					continue; 
				}
			}
		}
		
	}
	
	@Override
	public void digitalBankSignUp() {
		
		// Signing in will create a user and an account.
		
		showMenu.digitalBankSignUp();
		
		Client signUpClient = new Client(); //Name, birthdate, NIF, phone number, cell number, email, job description
		Account newAccount = new Account(); // Only balance will be needed. The rest is either auto-generated (account number) or is from client information
		
		// Name input
		String name;
		while (true) {
			
			System.out.print("Name >> ");
			input.nextLine(); // input error is occurring - line jumps - this is needed
			name = input.nextLine();
			
			try { // overshot. This "error" handle doesn't need to be so eloquent
				if (name.equals("") || name == null) { throw new Exception("Null input"); }
			} catch (Exception e) {
				continue; // When no user input is done, just continues in while.
			}
			break;
		}
		signUpClient.setName(name);
		System.out.println();
		
		// DOB input
		String dob;
		LocalDate dobParsed;
		while (true) {
			
			System.out.print("Birthdate (format: YYYY-MM-DD; must be overage) >> ");
			dob = input.nextLine();
			
			try { // this helps to handle general input errors.
				if (dob.equals("") || dob == null) { throw new Exception("Null input"); } // just for some error handling
				
				dobParsed = LocalDate.parse(dob);
				
				if (!isEighteen(dobParsed)) {
					System.out.println();
					System.out.println("» ERROR: client cannot be underage «");
					System.out.println();
					continue;
				}
				break;
				
			} catch (DateTimeParseException e) {
				System.out.println();
				System.out.println("» ERROR: incorrect date format or information. Correct format (numbers): YYYY-MM-DD «");
				System.out.println();
				continue;
			} catch (Exception e) {
				continue;
			}
		}
		signUpClient.setBirthdate(dobParsed);
		System.out.println();

		// NIF input
		String nif;
		Integer parsedNif; // cannot start with 0!!!
		while (true) {
			
			System.out.print("NIF (9 digits) >> ");
			
			try { // this helps to handle general input errors.
				nif = input.nextLine();
				
				if (nif.equals("") || nif == null) { continue; } // this empty input error is handled in Exception, but in this case, Exception is already used
				
				if (Integer.parseInt(nif.substring(0, 1)) == 0 || nif.length() != 9) { // if no input, will throw StringIndexOutOfBoundsException, because parse will fail
					throw new Exception ("Invalid number"); // if 1st number is 0 or number length is not 9, will throw Exception
				}
				
				parsedNif = Integer.parseInt(nif); // if the parse fails, will throw NumberFormatException
				break;
				
			} catch (NumberFormatException e) {
				System.out.println();
				System.out.println("» ERROR: incorrect input «");
				System.out.println();
				continue;
			} catch (StringIndexOutOfBoundsException e) {
				continue; // if input is null (no input was done, just carriage return), just continues;
			} catch (Exception e) {
				System.out.println();
				System.out.println("» ERROR: invalid NIF «");
				System.out.println();
				continue;
			}

		}
		signUpClient.setNif(parsedNif);
		System.out.println();
		
		// Phone number input
		String phoneNumber;
		Integer parsedPhoneNumber; // cannot start with 0!!!
		while (true) {
			
			System.out.print("Phone number (9 digits) >> ");
			
			try {
				phoneNumber = input.nextLine();
				
				if (Integer.parseInt(phoneNumber.substring(0, 1)) == 0 || phoneNumber.length() != 9) { // if no input, will throw StringIndexOutOfBoundsException because parse will fail
					throw new Exception ("Invalid number"); // if 1st number is 0 or number length is not 9, will throw Exception
				}
				
				parsedPhoneNumber = Integer.parseInt(phoneNumber); // if the parse fails, will throw NumberFormatException
				break;
				
			} catch (NumberFormatException e) {
				System.out.println();
				System.out.println("» ERROR: incorrect input «");
				System.out.println();
				continue;
			} catch (StringIndexOutOfBoundsException e) {
				continue; // if input is null (no input was done, just carriage return), just continues;
			} catch (Exception e) {
				System.out.println();
				System.out.println("» ERROR: invalid phone number «");
				System.out.println();
				continue;
			}
		}
		signUpClient.setPhoneNumber(parsedPhoneNumber);
		System.out.println();
		
		// Cell number input
		String cellNumber;
		Integer parsedCellNumber; // cannot start with 0!!!
		while (true) {
			
			System.out.print("Cell number (9 digits) >> ");
			
			try {
				cellNumber = input.nextLine();
				
				if (Integer.parseInt(cellNumber.substring(0, 1)) == 0 || cellNumber.length() != 9) { // if no input, will throw StringIndexOutOfBoundsException because parse will fail
					throw new Exception ("Invalid number"); // if 1st number is 0 or number length is not 9, will throw Exception
				}
				
				parsedCellNumber = Integer.parseInt(cellNumber); // if the parse fails, will throw NumberFormatException
				break;
				
			} catch (NumberFormatException e) {
				System.out.println();
				System.out.println("» ERROR: incorrect input «");
				System.out.println();
				continue;
			} catch (StringIndexOutOfBoundsException e) {
				continue; // if input is null (no input was done, just carriage return), just continues;
			} catch (Exception e) {
				System.out.println();
				System.out.println("» ERROR: invalid phone number «");
				System.out.println();
				continue;
			}
		}
		signUpClient.setCellNumber(parsedCellNumber);
		System.out.println();
		
		// Email input
		String email;
		while (true) {
			System.out.print("E-mail >> ");
			
			try {
				email = input.nextLine();
				
				if (email.equals("")) { throw new Exception("Null input"); } // just for some error handling
				
				// some email input validation
				if (email.indexOf("@") == email.length() - 1 || email.indexOf("@") == 0) {
					throw new UnknownFormatFlagsException("Invalid format");
				}
				if (email.indexOf(".") == email.length() - 1 || email.indexOf(".") == 0) {
					throw new UnknownFormatFlagsException("Invalid format");
				}
				if (email.length() < 9) { // chars considered in an email: 1(@) + 1(.) + 3(domain) + 2(domain extension) + 1(user) = 1+1+3+2+1 = 8
					throw new UnknownFormatFlagsException("Invalid format");
				}
				
				break;
			} catch (UnknownFormatFlagsException e) { // find another error to throw. This is not very accurate
				System.out.println();
				System.out.println("» ERROR: invalid email «");
				System.out.println();
				continue;
			} catch (Exception e) {
				continue; // When no user input is done, just continues in while.
			}
		}
		signUpClient.setEmail(email);
		System.out.println();
		
		// Job description input
		String job;
		while (true) {
			
			System.out.print("Job description >> ");
			job = input.nextLine();
			
			try { // overshot. This "error" handle doesn't need to be so eloquent
				if (job.equals("")) { throw new Exception("Null input"); }
			} catch (Exception e) {
				continue; // When no user input is done, just continues in while.
			}
			break;
		}
		signUpClient.setJob(job);
		System.out.println();
		
		// password input
		String password;
		while (true) {
			
			System.out.print("Password >> ");
			password = input.nextLine();
			
			try { // overshot. This "error" handle doesn't need to be so eloquent
				if (password.equals("")) { throw new Exception("Null input"); }
			} catch (Exception e) {
				continue; // When no user input is done, just continues in while.
			}
			break;
			
		}
		signUpClient.setPassword(password);
		System.out.println();

		// Client is finished. Saving him/her
		Client savedClient = clientservice.save(signUpClient); // this is needed to retrieve client Id to set up the account
		
		if (savedClient == null) { // nif is not unique
			System.out.println();
			System.out.println("» ERROR: NIF is already registered (client/account not created) «");
			System.out.println();
			return;
		}
			
		// Account balance
		String balance;
		Double parsedBalance;
		
		while (true) {
			System.out.print("Account inicial balance (minimum 50,00) >> ");
			balance = input.nextLine();
			
			try { // overshot. This "error" handle doesn't need to be so eloquent
				if (balance.equals("") || balance == null) { throw new Exception("Null input"); }
				
				parsedBalance = Double.parseDouble(balance); // if not parsed, will throw NumberFormatException
				
				if (parsedBalance < 50D ) {
					System.out.println();
					System.out.println("» ERROR: amount not valid «");
					System.out.println();
					continue;
				}
				
			} catch (NumberFormatException e) {
				System.out.println();
				System.out.println("» ERROR: incorrect input «");
				System.out.println();
				continue;
			} catch (Exception e) {
				continue; // When no user input is done, just continues in while.
			}
			break;
				
		}
		newAccount.setBalance(parsedBalance);
		newAccount.setId(savedClient.getId());
		//List<Long> startList = new ArrayList<Long>(Arrays.asList(0L));
		//newAccount.setSecondaryOwners(new ArrayList<Long>(Arrays.asList(-1L)));
		// Having a client and a balance, sign up with the bank can be finished
		Account savedAccount = accountservice.add(newAccount);
		
		System.out.println();
		System.out.println("  [ SIGN-UP ] >> New client and account setup is finished <<");
		System.out.println();
		System.out.println("  [ CLIENT DATA ] Name: " + savedClient.getName());
		System.out.println("                  Birthdate: " + savedClient.getBirthdate());
		System.out.println("                  Contacts: (phone) " + savedClient.getPhoneNumber() + ", (cell) " + savedClient.getCellNumber());
		System.out.println("                            (email) " + savedClient.getEmail());
		System.out.println();
		System.out.println("  [ CLIENT SIGN-IN DATA ] NIF:      " + savedClient.getNif());
		System.out.println("                          PASSWORD: " + savedClient.getPassword());
		System.out.println();
		System.out.println("  [ CLIENT ACCOUNT DATA ] Number: " + savedAccount.getAccountNumber());
		System.out.println("                          Balance: " + savedAccount.getBalance() + "");
		System.out.println();
		System.out.println("  >> Please don't forget your SING-IN data <<");

		
		// Leaves the current user logged
		//RdbApp.activeClientId = savedClient.getId(); // might not be needed if all management is done here
		currentClient = savedClient;
		currentClientId = currentClient.getId();
		
		System.out.println("        (Press enter to continue)");
		input.nextLine(); // for "enter press"
		
	}
	
	@Override
	public void digitalBankFront() { // This method can NEVER be called without a client sign in
		
		showMenu.digitalBankFront(this.currentClient.getName());
			
		Integer digitalBankFront = null;
		Boolean menuCycle = true;
		
		while (menuCycle) {

			System.out.print(">> ");
			
			try {
				digitalBankFront = input.nextInt();
				if (digitalBankFront.equals("") || digitalBankFront == null) { throw new Exception("Null input"); }
		
			}   catch (InputMismatchException e) {
				System.out.println();
				System.out.println("» ERROR: incorrect input «");
				System.out.println();
				input.next(); // clear scanner so that an infinite loop doesn't occur
				continue;
			} catch (Exception e) {
				input.next(); // to avoid infinite loops
				continue;
			}
			

			switch (digitalBankFront) {
			case 0: // exit. Uses stack to go back to digital bank signin/signup menu
				System.out.println();
				menuCycle = false;
				showMenu.digitalBankMain();
				break;
			case 1: // list accounts
				digitalBankFrontListAccountsAndCards();
				break;
			case 2: // list transactions
				break;
			case 3: // Create new account
				digitalBankFrontCreateNewAccount();
				break;
			case 4: // Add client to account
				digitalBankFrontAddClient();
				break;
			case 5: // deposit cash
				digitalBankFrontDepositCash();
				break;
			case 6:
				break;
			case 7:
				break;
			case 8:
				break;
			case 9:
				break;
			case 10:
				break;
			}
		}
		
	}
	
	private void digitalBankFrontListAccountsAndCards() {
		String titleHeader = "[ " + currentClient.getName() + " ] >> ";
		
		System.out.println();
		System.out.println(titleHeader + "MAIN ACCOUNTS: "+ allCurrentClientAccounts(currentClientId).size() + " <<");

		for (Account accounts : allCurrentClientAccounts(currentClientId)) {
			System.out.println(accounts.getAccountNumber() + " (Balance: " + accounts.getBalance() + ")");
		}
		
		if (allCurrentClientSecondaryAccounts(currentClientId) != null) {
			System.out.printf("%" + titleHeader.length() + "s", "SECONDARY ACCOUNTS: " + allCurrentClientSecondaryAccounts(currentClientId).size());
			System.out.println();
			
			for (Account accounts : allCurrentClientSecondaryAccounts(currentClientId)) {
				System.out.println(accounts.getAccountNumber() + " (Balance: " + accounts.getBalance() + ")");
			}
		}
		//list cards
		// 18/04/2022 still nothing
		System.out.println();
	}
	
	
	private void digitalBankFrontCreateNewAccount() {
		String balance;
		Double parsedBalance = null;
		Account newAccount = new Account();
		
		System.out.println("[ CREATING NEW ACCOUNT ] >> Enter the initial balance amount <<");
		System.out.println("          (Type 'quit' to exit: no account is created)");
		System.out.println();
		
		Boolean accountSetupSucess = true;
		
		while (true) {
			System.out.print("Account inicial balance (minimum 50,00) >> ");
			balance = input.nextLine();
			
			if (balance.equals("quit")) { accountSetupSucess = false;
				break; }
			
			try { // overshot. This "error" handle doesn't need to be so eloquent
				if (balance.equals("") || balance == null) { throw new Exception("Null input"); }
				
				parsedBalance = Double.parseDouble(balance); // if not parsed, will throw NumberFormatException
				
				if (parsedBalance < 50D ) {
					System.out.println();
					System.out.println("» ERROR: amount not valid «");
					System.out.println();
					continue;
				}
				
			} catch (NumberFormatException e) {
				System.out.println();
				System.out.println("» ERROR: incorrect input «");
				System.out.println();
				continue;
			} catch (Exception e) {
				continue; // When no user input is done, just continues in while.
			}	
			accountSetupSucess = true;
			break;
		}
		
		if (accountSetupSucess == true) {
			newAccount.setId(this.currentClientId);
			newAccount.setBalance(parsedBalance);
			accountservice.add(newAccount);
			System.out.println();
			System.out.println("  [ CLIENT ACCOUNT DATA ] Number: " + allCurrentClientAccounts(currentClientId).get(allCurrentClientAccounts(currentClientId).size()-1).getAccountNumber()); //always the last account added
			System.out.println("                          Balance: " + allCurrentClientAccounts(currentClientId).get(allCurrentClientAccounts(currentClientId).size()-1).getBalance());
		}
	}
	
	private void digitalBankFrontDepositCash() { // method can only be called if an account exists!!!

		System.out.println();
		System.out.println("  [ CURRENT CLIENT ACCOUNTS ] ");
		
		List<Account> existingAccounts = new ArrayList<Account>();
		Account accountToUpdate = new Account();
		
		for (Account accounts : allCurrentClientAccounts(currentClientId)) {
			System.out.println("   " + accounts.getAccountNumber());
			//existingAccounts.add(accounts);
		}
		
		if (allCurrentClientAccounts(currentClientId).size() != 1) { // More than one account. Must select one
			System.out.println();
			
			String accountNumber;
			
			while (true) {
				System.out.print("Enter account number >> ");
				// 16
				accountNumber = input.nextLine().strip();
				input.nextLine(); // it is jumping. needs this line
				
				if (accountNumber.length() != 16) {
					System.out.println();
					System.out.println("» ERROR: incorrect input «");
					System.out.println();
					continue;
				}
				
				for (Account accounts : allCurrentClientAccounts(currentClientId)) {
					if (accounts.getAccountNumber().equals(accountNumber)) {
						accountToUpdate = accounts;
					}
				}
				if (accountToUpdate == null) {
					// account number was not found
					System.out.println();
					System.out.println("» ERROR: invalid account number «");
					System.out.println();
					continue;
				} else { break; }
			}
		} else {
			// Cast is not working!!!
			//accountToUpdate = (Account) accountservice.find(currentClientId);
			for (Account accounts : allCurrentClientAccounts(currentClientId)) {
				accountToUpdate = accounts;
			}
		}
		
		String deposit;
		Double parsedDeposit;
		
		System.out.println();
		while (true) {
			
			System.out.print("Enter amount to deposit () >> ");
			deposit = input.nextLine();	
			
			try { // overshot. This "error" handle doesn't need to be so eloquent
				if (deposit.equals("") || deposit == null) { throw new Exception("Null input"); }
				
				parsedDeposit = Double.parseDouble(deposit); // if not parsed, will throw NumberFormatException

			} catch (NumberFormatException e) {
				System.out.println();
				System.out.println("» ERROR: incorrect input «");
				System.out.println();
				continue;
			} catch (Exception e) {
				continue; // When no user input is done, just continues in while.
			}
			
			break; // all ok in the try block: we have the amount to deposit
		}	
		// finish the deposit. use service
		accountservice.increaseBalance(accountToUpdate, parsedDeposit);
	}
	
	private void digitalBankFrontAddClient() {

		System.out.println();
		System.out.println("  [ CURRENT CLIENT ACCOUNTS ] ");
		
		List<Account> existingAccounts = new ArrayList<Account>();
		Account accountToAddSecondary = new Account();
		
		for (Account accounts : allCurrentClientAccounts(currentClientId)) {
			System.out.println("   " + accounts.getAccountNumber());
			//existingAccounts.add(accounts);
		}
		
		if (allCurrentClientAccounts(currentClientId).size() != 1) { // More than one account. Must select one
			System.out.println();
			
			String accountNumber;
			
			while (true) {
				System.out.print("Enter account number >> ");
				// 16
				accountNumber = input.nextLine().strip();
				input.nextLine(); // it is jumping. needs this line
				
				if (accountNumber.length() != 16) {
					System.out.println();
					System.out.println("» ERROR: incorrect input «");
					System.out.println();
					continue;
				}
				
				for (Account accounts : allCurrentClientAccounts(currentClientId)) {
					if (accounts.getAccountNumber().equals(accountNumber)) {
						accountToAddSecondary = accounts;
					}
				}
				
				if (accountToAddSecondary == null) {
					// account number was not found
					System.out.println();
					System.out.println("» ERROR: invalid account number «");
					System.out.println();
					continue;
				} else { break; }
			}
		} else {
			for (Account accounts : allCurrentClientAccounts(currentClientId)) { // this should get only one account
				accountToAddSecondary = accounts;
			}
		}

		List<Long> secondaryAccountIds = new ArrayList<Long>();
		
		// checks if number of secondary clients existing is within the limit
		if (accountToAddSecondary.getSecondaryOwners() != null) {
			if (accountToAddSecondary.getSecondaryOwners().size() >= numberOfSecondaryClients) {
				System.out.println();
				System.out.println("» ERROR: no more Clients can be added to this account «");
				System.out.println();
				return;
			}
			secondaryAccountIds = new ArrayList<Long>(accountToAddSecondary.getSecondaryOwners());
		}
		
		
		String nif;
		Integer parsedNif;
		
		System.out.println();
		
		while (true) {
			
			System.out.print("Enter Client NIF to add (type 'quit' to exit) >> ");
			input.nextLine();
			nif = input.nextLine().strip();	
			
			System.out.println();
			
			if (nif.toString().toLowerCase() == "quit") { break; }
			
			try { // overshot. This "error" handle doesn't need to be so eloquent
				
				if (nif.equals("") || nif == null) { throw new Exception("Null input"); }
				
				parsedNif = Integer.parseInt(nif);
				
				if (parsedNif.equals(currentClient.getNif())) {
					System.out.println();
					System.out.println("» ERROR: NIF not valid «");
					System.out.println();
					continue;
				}
				
			}  catch (NumberFormatException e) {
				System.out.println();
				System.out.println("» ERROR: incorrect input «");
				System.out.println();
				continue;
			} catch (Exception e) {
				continue; // When no user input is done, just continues in while.
			}
			
			
			//checks if NIF exists
			
			List<Client> clients = new ArrayList<Client>(clientservice.getAll());
			Long secondaryClientId = null;
			
			for (int i = 0; i < clients.size(); i++) {
				if (parsedNif.equals(clients.get(i).getNif())) {
					secondaryClientId = clients.get(i).getId();
				}
			}
			
			if (secondaryClientId == null) { // no client found;
				System.out.println();
				System.out.println("» ERROR: no client found «");
				System.out.println();
				continue;
			} else { 
				secondaryAccountIds.add(secondaryClientId);
				accountToAddSecondary.setSecondaryOwners(secondaryAccountIds);
				accountservice.update(accountToAddSecondary);
				System.out.println(accountToAddSecondary);
				break;
			}
		}
	}

	
	
	
	
	private boolean isEighteen(LocalDate dob) {

		LocalDate now = LocalDate.now();
		Period period = dob.until(now);

		if (period.getYears() >= 18) {
			return true;
		} 
		return false;
	}
	
	private List<Account> allCurrentClientAccounts(Long clientId) {
		List accounts = new ArrayList();
		
		if (accountservice.getAll().size() != 0) {
			for (Account account : accountservice.getAll()) {
				if (account.getId() == clientId) {
					accounts.add(account);
				}
			}
		} else { return null; }
		
		return accounts;
	}

	private List<Account> allCurrentClientSecondaryAccounts(Long clientId) {
		List<Account> accounts;

		if (accountservice.getAll().size() != 0) {
			if (accountservice.findSecondaries(clientId) != null ) {
				accounts = new ArrayList<Account>(accountservice.findSecondaries(clientId));
			} else { return null; }
		} else { return null; }
		
		return accounts;
	}
}
