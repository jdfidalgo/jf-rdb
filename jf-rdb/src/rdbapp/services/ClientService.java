package rdbapp.services;

import java.util.List;

import rdbapp.model.Client;

public interface ClientService {
	
	public Client save(Client newClient);

	public Client update(Client currentClient, Client newInformation);

	public Client find(Long clientId);
	
	public List<Client> getAll();

	public Boolean delete(Long clientId); // when possible, methods will return true or false to state if operation was successful
}
