package rdbapp.services;

import rdbapp.model.Client;

import java.time.LocalDate;
import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import rdbapp.database.ClientDatabase;
import rdbapp.database.ClientDatabaseImpl;

public class ClientServiceImpl implements ClientService {

	private ClientDatabase clientdatabase = new ClientDatabaseImpl();
	
	@Override
	public Client save(Client newClient) {
		// id will always be managed by database. It is not needed to be provided
		
		if (!isEighteen(newClient.getBirthdate())) { // Client is under age. This must always be checked
			return null;
		}
		
		if (getAll().size() != 0 && !isNifUnique(newClient.getNif())) { // if database is not null, clients exist, so unique nif check must be done
				return null;
		}

		clientdatabase.save(newClient);
		return newClient;
	}

	@Override
	public Client update(Client currentClient, Client newInformation) {

		if (!isEighteen(newInformation.getBirthdate())) { // Client is under age. This must always be checked
			return null;
		}
		
		currentClient = newInformation;
		clientdatabase.update(currentClient);
		return currentClient;
	}

	@Override
	public Client find(Long clientId) {
		return clientdatabase.find(clientId);
	}
	
	@Override
	public List<Client> getAll() {
		return clientdatabase.getAll();
	}

	@Override
	public Boolean delete(Long clientId) {
		// must be checked if this method can be called with an empty database.
		// if not, remove from here and ClientDatabaseImpl the null database checks.
		
		Client clientToDelete = find(clientId);
		
		if (clientToDelete == null) { return false; } // no client found
		
		clientdatabase.delete(clientId);
		return true;
	}
	
	private boolean isEighteen(LocalDate dob) {

		LocalDate now = LocalDate.now();
		Period period = dob.until(now);

		if (period.getYears() >= 18) {
			return true;
		} 
		return false;
	}

	private Boolean isNifUnique(Integer checkNif) { // method must be checked for null database when called
		
		for (int i = 0; i < getAll().size(); i++) {
			
			if (getAll().get(i).getNif().equals(checkNif)) {
				return false; // finds one equal nif, then returns false - provided nif isn't unique
			}
		}
		return true; // if no previous check or comparison turned up not valid or unique, then its a unique number
	}
}
