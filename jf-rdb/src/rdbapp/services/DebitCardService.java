package rdbapp.services;

import java.util.List;

import rdbapp.model.DebitCard;

public interface DebitCardService {

	public DebitCard add(DebitCard newDebitCard);
	
	public Boolean remove(String debitCardNumber);
	
	public Boolean setPinNumber(DebitCard debitCard, String pinNumber);
	
	public DebitCard find(Long clientId);
	
	public List<DebitCard> getAll(Long clientId);
	
}
