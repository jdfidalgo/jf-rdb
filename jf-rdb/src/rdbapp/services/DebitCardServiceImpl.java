package rdbapp.services;

import java.util.List;

import rdbapp.database.DebitCardDatabase;
import rdbapp.database.DebitCardDatabaseImpl;
import rdbapp.model.DebitCard;

public class DebitCardServiceImpl implements DebitCardService {

	private DebitCardDatabase debitcarddatabase = new DebitCardDatabaseImpl();
	
	@Override
	public DebitCard add(DebitCard newDebitCard) {
		return debitcarddatabase.add(newDebitCard);
	}

	@Override
	public Boolean remove(String debitCardNumber) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean setPinNumber(DebitCard debitCard, String pinNumber) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DebitCard find(Long clientId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<DebitCard> getAll(Long clientId) {
		// TODO Auto-generated method stub
		return null;
	}

}
