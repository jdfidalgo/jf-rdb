package rdbapp.ui;

import java.time.LocalDate;

import rdbapp.model.Client;

public class DataSeeder {
	
//	private Long id;
//
//	// will be used for login
//	private Integer nif; // cannot start with 1
//	private String password;
//
//	// the rest of client "properties"
//	private String name;
//	private LocalDate birthdate; // format is YYYY-MM-DD
//	private Integer phonenumber; // 9 digits - must start with 2
//	private Integer cellnumber; // 9 digits - must start with 9
//	private String email;
//	private String job;
	
	public void clients() {
		// seed 5 clients
		Client seedClients = new Client();
		
		Long[] ids = new Long[] {10001L, 10002L, 10003L, 10004L, 10005L};
		
		Integer[] nifs = new Integer[] {216214567, 212394023, 239940495, 237890340, 223789402};
		String[] passwors = new String[] {"0000", "1111", "1111", "1111", "1111"};
		String[] names = new String[] { "Jo�o Fidalgo", "Isabel Leit�o", "Ant�nio Costa", "Rui Rio", "Andr� Ventura"};
		LocalDate[] birthdates = new LocalDate[] {LocalDate.of(1980, 7, 25), LocalDate.of(1983, 10,29), LocalDate.of(1960, 1, 1), LocalDate.of(1970, 1,1), LocalDate.of(1980,1,1)};
		
		Integer[] phonenumbers = new Integer[] {213839303, 229393485, 235884849, 228993038, 219903945};
		Integer[] cellnumbers = new Integer[] {911029494, 923894738, 918839392, 913030345, 912299345};
		
		String[] emails = new String[] {"joao@fidalgo.com", "isabel@leitao.com", "antonio@costa.com", "rui@rio.com", "andre@ventura.com"};
		String[] jobs = new String[] {"engenheiro", "engenheiro", "politico", "politico", "politico"};
		
		for (int i = 1; i <= 5; i++ ) {
			
		}
	}
}
