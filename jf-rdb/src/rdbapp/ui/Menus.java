package rdbapp.ui;

public interface Menus {
	
	public void main();
	
	public void digitalBankMain();

	public void digitalBankSignUp();
	
	public void digitalBankSignIn();
	
	public void digitalBankFront(String clientName);
	
	public void atmMain();
}
