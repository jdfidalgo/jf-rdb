package rdbapp.ui;

import rdbapp.model.Client;

public class MenusImpl implements Menus{

	@Override
	public void main() {
		
		System.out.println();
		
		System.out.println("  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
		System.out.println("  %%                                           %%");
		System.out.println("  %%%%%%%%%%% WELCOME TO RDB SERVICES %%%%%%%%%%%");
		System.out.println("  %%                                           %%");
		System.out.println("  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
		System.out.println();
		System.out.println("   [ 1 ] DIGITAL BANK    [ 2 ] ATM    [ 0 ] EXIT");
		
		System.out.println();

		
	}

	@Override
	public void digitalBankMain() {
		
		System.out.println();
		System.out.println("  ##########################################################");
		System.out.println("  ##                                                      ##");
		System.out.println("  ################## DIGITAL BANK SERVICE ##################");
		System.out.println("  ##                                                      ##");
		System.out.println("  ##                          RDB                         ##");
		System.out.println("  ##########################################################");
		System.out.println("  ##                                                      ##");
		System.out.println("  ## [ 1 ] SIGN-IN    [ 2 ] SIGN-UP    [ 0 ] EXIT SERVICE ##");
		System.out.println("  ##                                                      ##");
		System.out.println("  ##########################################################");
		System.out.println();
		
		
	}
	
	@Override
	public void digitalBankSignUp() {
		
		System.out.println();
		System.out.println("  [ SIGN-UP ] >> Please enter the following personal information <<");
		System.out.println("  >> Name, birthdate, NIF, phone number, cell number, email, job description <<");
		System.out.println();

	}
	
	@Override
	public void digitalBankSignIn() {
		System.out.println();
		System.out.println("  [ SIGN-IN ] >> Please enter your NIF and password <<");
		System.out.println("  >> You have 3 attempts to sign-in <<");
		System.out.println();
	}
	
	@Override
	public void digitalBankFront(String clientName) {

		// 56 is the length of header
		int spacing = 0; // The last string for title will be #. If name is too long, only "Welcome" will be shown
		StringBuilder titleSettingStr = new StringBuilder("  # Welcome ");
		
		spacing = 56 - titleSettingStr.length() - clientName.length();
		
		if (spacing <= 1 ) {
			titleSettingStr.append("                                           #\n"); //default for long name
		} else {
			titleSettingStr.append("%s%" + spacing + "s\n");
		}
		
		System.out.println();
		System.out.println("  ######################################################");
		System.out.println("  ################ DIGITAL BANK SERVICE ################");
		System.out.println("  ################       FRONT END      ################");
		System.out.println("  ######################################################");
		System.out.println("  #                                                    #");
		System.out.printf(titleSettingStr.toString(),clientName,"#");
		System.out.println("  #                                                    #");
		System.out.println("  #--------------------- ACCOUNTS, CARDS AND LISTINGS -#");
		System.out.println("  # [ 1  ] LIST ACCOUNTS AND CARDS                     #");
		System.out.println("  # [ 2  ] LIST ACCOUNTS AND CARDS TRANSACTIONS        #");
		System.out.println("  # [ 3  ] CREATE NEW ACCOUNT                          #");
		System.out.println("  # [ 4  ] ADD CLIENT TO ACCOUNT                       #");
		System.out.println("  #---------------------------------- CASH OPERATIONS -#");
		System.out.println("  # [ 5  ] DEPOSIT CASH                                #");
		System.out.println("  # [ 6  ] TRANSFER CASH                               #");
		System.out.println("  #---------------------------------- CARD OPERATIONS -#");
		System.out.println("  # [ 7  ] REQUEST CARDS                               #");
		System.out.println("  # [ 8  ] CANCEL CARDS                                #");
		System.out.println("  # [ 9  ] CHANGE CARD PIN NUMBER                      #");
		System.out.println("  #------------------------------------ PERSONAL DATA -#");
		System.out.println("  # [ 10 ] CHANGE PERSONAL DATA                        #");
		System.out.println("  #----------------------------------------------------#");
		System.out.println("  # [ 0  ] EXIT                                        #");
		System.out.println("  ######################################################");
		System.out.println();
	}

	@Override
	public void atmMain() {
		// TODO Auto-generated method stub
		
	}

}
